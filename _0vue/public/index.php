<?php
$url = urldecode($_SERVER['REQUEST_URI']);
$url_path = substr($url, strrpos($url, ".rs/")+1);
$url_path = explode('/',$url_path,3);

$grad = $url_path[0];
$sub_category = $url_path[1];
$name = $url_path[2];

$meta_common_title = "pokrenise.rs";
$meta_common_desc = "Sport, rekreacija, učenje jezika, obuke, kursevi...ko, gde, kada - sve informacije na jednom mestu - pokrenise.rs";
$meta_common_keys = "pokrenise,pokrenise.rs,webcodeandhost,Nikola Pavlović,Stevan Nikolić,informacije,termini,uzrast,cena,adresa,";

if (! $sub_category) {
    //pocetna stranica
    $meta_title = "POKRENISE.RS - sport, rekreacija, jezici, obuke... ko, gde, kada...";
    $meta_desc = $meta_common_desc;
    $meta_keys = $meta_common_keys."Sport,rekreacija,učenje jezika,obuke,kursevi,sve informacije na jednom mestu";
}
elseif (! $name) { 
    $name = $sub_category." ".$grad;
    // lista podkategorije...fudbal,kud...
    $meta_title = $meta_common_title." - ".$grad." - ";
    $meta_keys = $meta_common_keys.$sub_category.",".$grad.",";
            
    switch (getCat(mb_strtolower($sub_category, 'UTF-8'))) {
        case 'sport':
            $meta_title .= $sub_category . " - sve informacije o treninzima";
            $meta_desc =  "Želiš da treniraš ".getPadezZaSport($sub_category)." u ".getPadezZaGrad($grad)." - pokrenise web sajt ima sve informacije koje su ti potrebne - imena klubova, trenera, termini, lokacije, cene...";
            $meta_keys.="gde da treniram ".$sub_category.",";
            $meta_keys.="klub,klubovi,trener,treneri";
            break;
        case 'umetnost':   
            $meta_title .= $sub_category . " - sve informacije o terminima";  
            $temp =   $sub_category == "KUD" ? "kulturno umetničko duštvo"  : $sub_category;           
            $meta_desc =  "Zanima te ".$temp." u ".getPadezZaGrad($grad)." - ko drži časove, za koji uzrast, kada i koliko - sve potrebne informacije možeš pronaći na pokrenise web sajtu...";
            $meta_keys.="gde da naučim ".$sub_category.",".$temp.",";
            $meta_keys.="klub,klubovi,predavači";
            break;
        case 'strani jezici':
            $meta_title .= "saznaj ko,gde,kada drži časove ";                    
            $meta_title .= substr_replace($sub_category, "", -1)."og"; 
            $meta_desc =  "Želiš da naučiš ".$sub_category." jezik u ".getPadezZaGrad($grad)." - na sajtu pokrenise.rs imaš sve potrebne informacije - koje su škole stranih jezika, predavači, termini...";
            $meta_keys.="gde da naučim ".$sub_category.",";
            $meta_keys.="kurs ".$sub_category." jezika u ".getPadezZaGrad($grad).",";
            $meta_keys.="predavači";
            break;
        case 'dodatna nastava':
            $meta_title .= $sub_category . " - saznaj ko,gde,kada drži časove";
            $meta_desc =  "Trebaju ti dodatni časovi iz ".getPadezZaNastavu($sub_category)." u ".getPadezZaGrad($grad).", za osnovnu ili srednju školu, ili priprema za polaganje prijemog ispita - pokrenise ima sve informacije...";
            $meta_keys.="dodatni časovi iz ".getPadezZaNastavu($sub_category)." u ".getPadezZaGrad($grad).",";
            $meta_keys.="predavači";                    
            break;
        case 'obuke i kursevi':
            $meta_title .= $sub_category . " - sve informacije o obukama";
            $meta_desc =  "Želiš da naučiš ".$sub_category." u ".getPadezZaGrad($grad)." - na sajtu pokrenise.rs imaš sve potrebne informacije - ko vrši obuku, gde, kada, za koji uzrast, cene...";
            $meta_keys.="obuka za ".$sub_category." u ".getPadezZaGrad($grad).",";
            $meta_keys.="predavači";
            break;            
        default:
            $meta_title .= "sve informacije na jednom mestu";
            $meta_desc = $meta_common_desc;
            break;
    }
}
else{
    $meta_title = $meta_common_title." - ".$grad." - ";
    $meta_keys = $meta_common_keys.$sub_category.",".$grad.",";

    switch (getCat(mb_strtolower($sub_category, 'UTF-8'))) {
        case 'sport':
            $meta_title .= "$sub_category - sve informacije o treninzima";
            $meta_desc =  "Želiš da treniraš ".getPadezZaSport($sub_category)." u $name u ".getPadezZaGrad($grad)." - pokrenise web sajt ima sve informacije koje su ti potrebne - termini, lokacije, cene...";
            $meta_keys.="gde da treniram $sub_category,klub,klubovi,trener,treneri";                    
            break;
        case 'umetnost':   
            $meta_title .= $sub_category . " - sve informacije o terminima";  
            $temp =   $sub_category == "KUD" ? "kulturno umetničko duštvo"  : $sub_category;           
            $meta_desc =  "Zanima te $temp u $name u ".getPadezZaGrad($grad)." - za koji uzrast, kada i cene - sve potrebne informacije možeš pronaći na pokrenise web sajtu...";
            $meta_keys.="gde da naučim $sub_category, $temp,klub,klubovi,predavači";
            break;
        case 'strani jezici':
            $meta_title .= "saznaj ko, gde, kada drži časove "; 
            $temp = substr_replace($sub_category, "", -1)."og jezika";     
            $meta_title .= $temp; 
            $meta_desc =  "$name - škola $temp u ".getPadezZaGrad($grad)." - na sajtu pokrenise.rs imaš sve potrebne informacije - lokacija, termini, cene...";
            $meta_keys.="gde da naučim $sub_category,kurs $temp u ".getPadezZaGrad($grad).",predavači";
            break;
        case 'dodatna nastava':
            $meta_title .= "$sub_category - saznaj ko, gde, kada daje časove";
            $meta_desc = "$name - časovi iz ".getPadezZaNastavu($sub_category)." u ".getPadezZaGrad($grad)." - pokrenise.rs ima sve informacije...";
            $meta_keys.="dodatni časovi iz ".getPadezZaNastavu($sub_category)." u ".getPadezZaGrad($grad).",predavači";                    
            break;
        case 'obuke i kursevi':
            $meta_title .= "$sub_category - sve informacije o obukama";
            $meta_desc = "$name nauči ".$sub_category." u ".getPadezZaGrad($grad)." - na sajtu pokrenise.rs imaš sve potrebne informacije - gde se održava obuka, kada, za koji uzrast, cene...";
            $meta_keys.="obuka za ".$sub_category." u ".getPadezZaGrad($grad).",";
            $meta_keys.="predavači";                    
            break;            
        default:
            $meta_title .= "sve informacije na jednom mestu";
            $meta_desc = $meta_common_desc;
            break;
    }
    
}
?>

<!DOCTYPE html>
<html lang="sr">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="robots" content="index, follow"> 
    <meta http-equiv="Cache-Control" content="public">
    <meta name="generator" content="Vue.js">
    <meta name="rating" content="General">
    <meta name="abstract" content="Sport, rekreacija, učenje jezika, obuke, kursevi...ko, gde, kada - sve informacije na jednom mestu">

    <title><?=$meta_title?></title>
    <meta name="description" content="<?=$meta_desc?>">
    <meta name="keywords" content="<?=$meta_keys?>">
    <meta property="og:title" content="<?=$meta_title?>">

    <meta itemprop="name" content="<?=$name?>">
    <meta name="twitter:title" content="<?=$name?>"><!-- koristi VIBER -->
    <meta itemprop="description" content="<?=$meta_desc?>">
    <meta name="twitter:description" content="<?=$meta_desc?>">
    <meta itemprop="image" content="/favicon-96x96.png">
    <meta name="twitter:image" content="/favicon-96x96.png">  

    <meta property="og:site_name" content="pokrenise.rs">
    <meta property="og:description" content="Sport, rekreacija, učenje jezika, obuke, kursevi...ko, gde, kada - sve informacije na jednom mestu">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://pokrenise.rs">
    <meta property="og:locale" content="RS">
    <meta property="og:image" content="https://pokrenise.rs/og_icon.png">
    <meta property="og:image:width" content="1537">
    <meta property="og:image:height" content="902">

    <meta name="ICBM" content="20.687254, 43.723848">
    <meta name="geo.position" content="20.687254;43.723848">
    <meta name="geo.region" content="RS">
    <meta name="geo.placename" content="Kraljevo">

    <meta name="author" content="WebCodeAndHost">
    <meta name="web_author" content="WebCodeAndHost">
    <meta name="copyright" content="WebCodeAndHost"> 
    <link rel="me" href="https://WebCodeAndHost.com" type="text/html">
    <link rel="me" href="mailto:nikola.webcodeandhost@gmail.com">

    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">

    <meta name="msapplication-TileColor" content="#846340">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#846340">
       
</head>

<?php
include "index.html";
/* echo "url: $url<br><br>";
echo "name: $name<br><br>";
echo "meta_title: $meta_title<br><br>";
echo "meta_desc: $meta_desc<br><br>";
echo "meta_keys: $meta_keys<br><br>"; */
?>

<?php
function getPadezZaGrad($out){
    switch ($out) {
        case "kraljevo": return "Kraljevu";
        case "beograd": return "Beogradu";            
        default: return $out;
    }
}
function getPadezZaSport($out){
    if ( substr($out, -1) == 'a') { //odbojka
        return substr_replace($out, "", -1) . "u"; //odbojku
    } else {
        return $out;
    }
}
function getPadezZaNastavu($out){
    if ( substr($out, -1) == 'a') { //matematika
        return substr_replace($out, "", -1) .  "e"; //matematike
    } else if (substr($out, -1) == 'i') { //srpski
        return substr_replace($out, "", -1) .  "og"; //srpskog
    } else {
        return $out;
    }
}
function getCat($sub_category){
    $niz_sport = array(
        "fudbal",
        "odbojka",
        "košarka",
        "tenis",
        "biciklizam",
        "planinarenje",
        "paraglajding",
        "plivanje",
        "gimnastika",
        "karate",
        "fitnes",
        "aerobik",
        "balon sale",
        "igraonice"
    );
    $niz_umetnost =array(
        "KUD",
        "kud",
        "pevanje",
        "ples",
        "gluma"
    );
    $niz_jezici =array(
        "engleski", 
        "nemački", 
        "italijanski"
    );
    $niz_nastava =array(
        "matematika",
        "fizika", 
        "hemija"
    );
    $niz_obuka =array(
        "računarstvo",
        "programiranje"
    );

    if (in_array($sub_category, $niz_sport)) {
        return "sport";
    } 
    elseif (in_array($sub_category, $niz_umetnost)){
        return "umetnost";
    }
    elseif (in_array($sub_category, $niz_jezici)){
        return "strani jezici";
    }
    elseif (in_array($sub_category, $niz_nastava)){
        return "dodatna nastava";
    }
    elseif (in_array($sub_category, $niz_obuka)){
        return "obuke i kursevi";
    }
    else{
        return "";
    } 
}
?>