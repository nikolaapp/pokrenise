import Vue from "vue";
Vue.config.productionTip = false;// DISABLE console showing: "You are running Vue in development mode."

if (window.location.href.indexOf('localhost') !== -1) {
    Vue.prototype.$controller_path = '/pokrenise/_control/controller.php';
} else {
    Vue.prototype.$controller_path = '/_control/controller.php';
}

Vue.prototype.$meta_common_title = "pokrenise.rs";
Vue.prototype.$meta_common_desc = "Sport, rekreacija, učenje jezika, obuke, kursevi...ko, gde, kada - sve informacije na jednom mestu - pokrenise.rs";
Vue.prototype.$meta_common_keys = "pokrenise,pokrenise.rs,webcodeandhost,Nikola Pavlović,Stevan Nikolić,informacije,termini,uzrast,cena,adresa,";

window.COUNTER_LOADS = 0;

export default {
    ga_id: 'UA-135431789-1',
    ga_debug: false,
    ga_enable_count: process.env.NODE_ENV === 'production',

 }
 