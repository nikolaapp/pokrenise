import Vue from "vue";
import Router from "vue-router";

/* import List from "./components/List.vue";
import ItemDetails from "./components/ItemDetails.vue"; */
//lazy loading
const List = () => import("./components/List.vue");
const ItemDetails = () => import("./components/ItemDetails.vue");
const NewItem = () => import("./components/NewItem.vue");

Vue.use(Router);

export default new Router({
    /* scrollBehavior() {
        return { x: 0, y: 0 };
    }, */
    mode: "history", /* removing # from url */
    /* umesto localhost/kraljevo/kosarka -> localhost/pokrenise/kraljevo/kosarka ---todo-jel to tacno??? */
    base: process.env.BASE_URL,
    /* scrollBehavior (to, from, savedPosition) {
        // page scroll to top for all route navigations
        return { x: 0, y: "-50px" }
    }, */
    /* \\scrollBehavior: (to, from, savedPosition) => {
        document.getElementById('app').scrollIntoView();
        return null;
    }, */
    routes: [
        {
            name: "root",
            path: "/",
            redirect: {
                name: "kraljevo"
            }
        },
        {
            name: "kraljevo",
            path: "/kraljevo",
            component: List,
            children: [
                {
                    name: "sub_category",
                    path: ":sub_category",
                    component: List,
                    beforeEnter: (to, from, next) => {
                        function isValid (param) {
                            // todo probaj da ovo ubrzas
                            //console.log(param); 
                            var bool = false;
                            getSubCats().forEach(element => {
                                //console.log(element[0]);                            
                                if (element[0] == param) {
                                    bool =  true;
                                }
                            });
                            if (bool == false) {
                                window.PORUKA = "Uneli ste neispravnu adresu: <strong>"+window.location.href+"</strong>";                            
                            }
                            return bool;
                        }
                     
                        if (!isValid(to.params.sub_category)) {
                            next({ name: 'root' });
                        }
                        else{
                            next();
                        }
                     
                    }
                }
            ]
        },
        {
            name: "oglasi",
            //mora ceo url (sa / na pocetku) da ne bi duplirao unose unutar url
            path: '/kraljevo/:sub_category/:oglas',
            component: ItemDetails,
            props: true
        },
        {
            name: "dodaj_oglas",
            //mora ceo url (sa / na pocetku) da ne bi duplirao unose unutar url
            path: '/kraljevo/dodaj_oglas',
            component: NewItem,
            props: true
        },
        {
            path: '*', 
            beforeEnter: (to, from, next) => {
                window.PORUKA = "Uneli ste neispravnu adresu: <strong>"+window.location.href+"</strong>";                            
                next({ name: 'root' });
            }
        }
    ]
});

function getSubCats() {
    return [
        ["fudbal", "fas fa-futbol"],
        ["odbojka", "fas fa-volleyball-ball"],
        ["košarka", "fas fa-basketball-ball"],
        ["tenis", "fas fa-baseball-ball"],
        ["biciklizam", "fas fa-bicycle"],
        ["planinarenje", "fas fa-hiking"],
        ["paraglajding", "fas fa-paper-plane"],
        ["plivanje", "fas fa-swimmer"],
        ["gimnastika", "fas fa-street-view"],
        ["karate", "fas fa-user-ninja"],
        ["fitnes", "fas fa-heartbeat"],
        ["aerobik", "fas fa-female"],
        ["balon sale", "fas fa-rainbow"],
        ["igraonice", "fas fa-baby"],
        ["ostalo", "fas fa-external-link-square-alt"],

        ["KUD", "fas fa-heartbeat"],
        ["pevanje", "fas fa-microphone-alt"],
        ["ples", "fas fa-shoe-prints"],
        ["gluma", "fas fa-hand-holding-heart"],
        ["ostalo", "fas fa-external-link-square-alt"],

        ["engleski", "fas fa-comments"],
        ["nemački", "fas fa-comment"],
        ["italijanski", "far fa-comments"],
        ["ostalo", "fas fa-external-link-square-alt"],

        ["matematika", "fas fa-superscript"],
        ["fizika", "fas fa-tachometer-alt"],
        ["hemija", "fas fa-atom"],
        ["ostalo", "fas fa-external-link-square-alt"],

        ["računarstvo", "fas fa-laptop"],
        ["programiranje", "fas fa-code"],
        ["ostalo", "fas fa-external-link-square-alt"],
        ["dodaj_oglas",""]
    ];
    
}
