import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import myconstants from './myconstants';

import "./plugins/fontawesome5/css/all.min.css";
/* import "../node_modules/animate.css/source/_base.css";
import "../node_modules/animate.css/source/attention_seekers/bounce.css"; */
import "./animate.css";

//import "./plugins/iview.js";
import iView from "iview";
import locale from "iview/dist/locale/en-US";
Vue.use(iView, { locale });
import "iview/dist/styles/iview.css";

import Lightbox from 'vue-pure-lightbox'
Vue.use(Lightbox)

import VueAnalytics from 'vue-analytics'
Vue.use(VueAnalytics, {
  id: myconstants.ga_id,
  debug: {
    enabled:    myconstants.ga_debug,
    sendHitTask: myconstants.ga_enable_count
  },
  autoTracking: {
    skipSamePath: true
  },
  router
})

router.beforeEach((to, from, next) => {
    iView.LoadingBar.start();
    next();
});
router.afterEach(route => {
    iView.LoadingBar.finish();
});

import vueHeadful from 'vue-headful';
Vue.component('vue-headful', vueHeadful);

// skracena notacija
new Vue({
    router,
    render: h => h(App)
}).$mount("#app");


/****** opsta notacija
new Vue({
    el: "#app",
    router,
    render: function (createElement) {
        return createElement(App);
    }
  });
*/