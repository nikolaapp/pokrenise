<?php

if (! defined('IS_FILE_INCLUDED' /*ovde mora pod navodnicima*/) ){
    // nije definisana konstanta IS_FILE_INCLUDED - definise se preko kontrolera
    exit(header( 'HTTP/1.0 404 Not Found', TRUE, 404 ));
}


class DB{
    private static $factory;
    
    public static function createInstance($config = null){  
        
        if ( defined('IS_LOCAL')  ) { // define constanta mora pod navodnicima
            //xampp localhost
            $settings['dbname'] = 'pokrenise';
            $settings['dbhost'] = '127.0.0.1';
            $settings['dbuser'] = 'root';
            $settings['dbpass'] = '';
            
            if (isset($user['dozvole'])){
                switch ($user['dozvole']) {
                    case 'admin':
                        $settings['dbuser'] = 'admin';
                        $settings['dbpass'] = 'KU1TDXSIiwTiYtIr';
                        break;
                    default:
                        $settings['dbuser'] = 'viewOnly';
                        $settings['dbpass'] = '60BMPsNAxq30RNb9';
                        break;
                } 
            }
        }
        else{
            $settings['dbname'] = 'webcodea_pokrenise';
            $settings['dbhost'] = 'localhost';
            $settings['dbuser'] = 'webcodea_nikola';
            $settings['dbpass'] = 'KV5,[cOGJ,fO';
        }
        
        try{
            $dsn = 'mysql:dbname=' . $settings['dbname'] . ';host=' . $settings['dbhost'];
            $pdo = new PDO($dsn, $settings['dbuser'], $settings['dbpass'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            
            self::$factory[$config] = $pdo;
            
            return self::$factory[$config];
        }
        catch (PDOException $e) {
            $greska = array(
                "errCode"=>988,
                "errMsg"=>"kontaktirajte administratora aplikacije" );
            return $greska;
        }
    }
}
?>
