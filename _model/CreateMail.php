<?php
/**
 * Kreiranje i slanje e-mail
 *
 * @author     Nikola Pavlovic, Kraljevo
 * @copyright  Nikola Pavlovic, Kraljevo
 * @since      06.04.2019.
 * @version    fileVer 1.0
 */
if (! defined('IS_FILE_INCLUDED' /*ovde mora pod navodnicima*/) ){
    // nije definisana konstanta IS_FILE_INCLUDED - definise se preko kontrolera
    exit(header( 'HTTP/1.0 404 Not Found', TRUE, 404 ));
}

require_once 'PHPMailer/PHPMailer.php';
require_once 'PHPMailer/SMTP.php';

function f_createMail($naziv, $url, $send_valdation, $upload_contact=NULL)
{
    $mail = new PHPMailer(true); // Passing `true` enables exceptions
    try {        
        $mail->isHTML(true); // Set email format to HTML
        $mail->CharSet = 'UTF-8';

        // Server settings
        $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
        $mail->Username = 'mpu.webapp'; // SMTP username
        $mail->Password = 'Nikola.036'; // SMTP password
        $mail->SMTPDebug = 0; // Enable verbose debug output
        $mail->isSMTP(); // Set mailer to use SMTP
        $mail->SMTPAuth = true; // Enable SMTP authentication
        $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587 ; // TCP port to connect to

        // Recipients
        $mail->setFrom('no-reply@gmail.com', 'pokrenise.rs');
        // $mail->addAddress('joe@example.net', 'Joe User'); // Add a recipient
        $mail->addAddress('kontakt@pokrenise.rs'); // Name is optional
        $mail->addAddress('stevan.webcodeandhost@gmail.com'); // Name is optional
        
        //$mail->addReplyTo('info@example.com', 'Information');
        //$mail->addCC('nikola.aoe2@gmail.com');
        //$mail->addBCC('mpu.webapp@gmail.com');
        
        // Content
        $subject = "$naziv - pokrenise.rs - dodat je nov oglas: ";
        $body = tekstMejla($naziv, $url, $send_valdation, $upload_contact);
        if ( defined('IS_LOCAL')  ){
             $subject ="localhost ".$subject;
             $body ="localhost ".$subject;
        }
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->AltBody = "Dodat je nov oglas: $naziv";
        
        $mail->send();
        return 0;

    } catch (Exception $e) {
        return 'Message could not be sent. Mailer Error: '. $mail->ErrorInfo;
    }
}

function tekstMejla($naziv, $url, $send_valdation, $upload_contact=NULL){
    $naslov = "<h2>$naziv - nov oglas na pokrenise.rs!</h2>";
    $podnaslov = "<p>Pogledaj oglase koji nisu još verifikovani: <a href='$url'>link</a></p>";
    $podnaslov .= "<br><p>Odobri oglas za <strong>$naziv</strong> klikom na link ispod: <br><a style='color:green;' href='$send_valdation'>ODOBRI OGLAS</a></p>";
    
    if ($upload_contact) {
        $podnaslov .= "<br><p>Kontakt korisnika koji je uneo oglas: ";
        if (strpos($upload_contact, '@') !== false) {
            $podnaslov .= "<a href='mailto:$upload_contact'>$upload_contact</a></p>";
        } else {
            $podnaslov .= "<a href='sms:$upload_contact'>$upload_contact</a></p>";
        }        
    }

    $footer = "<br><br><hr>";
    $footer .= "<div style='font-size:0.8em;'>"
        ."Upozorenje:<br>"
        ."Ova elektronska poruka i svi podaci/prilozi koji se putem nje prenose su poverljivi i namenjeni isključivo primaocu, odnosno pojedincu i/ili licima na koje je adresirana. Ova poruka može sadržati poverljive i/ili privilegovane informacije koje su zakonski zašticene od objavljivanja. Ukoliko ste ovu elektronsku poruku dobili greškom, obaveštavamo Vas da je bilo kakvo korišcenje, umnožavanje, širenje ili cuvanje ove poruke najstrože zabranjeno. Molimo da u tom slucaju odmah obavestite pošiljaoca poruke putem elektronske pošte ili telefonom, a zatim izbrišete poruku i sve njene priloge."    
        ."<br><br>"
        ."Warning:<br>"
        ."This electronic message and all data / contributions transmitted through it are confidential and are intended solely for the recipient, that is, the individual and / or the persons to whom it is addressed. This message may contain confidential and / or privileged information that is legally protected from publication. If you have received this email by mistake, we would like to inform you that any use, duplication, extension or storage of this message is strictly prohibited. In this case, please immediately inform the sender of the message by e-mail or phone, then delete the message and all its attachments."
    ."</div>";
    
    return $naslov.$podnaslov.$footer;
    
}

?>  