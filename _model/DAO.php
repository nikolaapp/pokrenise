<?php
/**
 * DataAccessObject za pristup bazi
 *
 * @author     Nikola Pavlovic, Kraljevo
 * @copyright  Nikola Pavlovic, Kraljevo
 * @since      04.02.2019.
 * @version    fileVer 1.0
 */

if (! defined('IS_FILE_INCLUDED' /*ovde mora pod navodnicima*/) ){
    // nije definisana konstanta IS_FILE_INCLUDED - definise se preko kontrolera
    exit(header( 'HTTP/1.0 404 Not Found', TRUE, 404 ));
}

f_requireOnce_handler(ROOT.'_model/db.php');

class DAO{
    // ako nema private/public onda je po def public
    private $db;
    function __construct(){
        $this->db = DB::createInstance();
        
        ### uhvacen Ecxeption
        if (is_array($this->db)){
            f_ajaxReturn($this->db["errCode"],$this->db["errMsg"]);
        }
    }
    /////////////////////////////////////////////////////////////
    
    private $SELECT_HOME_subCat = 
    'SELECT  oglasi.id, oglasi.grad, oglasi.naziv, oglasi.naziv_url, oglasi.category, oglasi.sub_category, oglasi.adresa, oglasi.mala_slika, oglasi.treneri, GROUP_CONCAT(DISTINCT grupe.uzrast_od, "-", grupe.uzrast_do SEPARATOR ", ") AS uzrast           
    FROM oglasi
    INNER JOIN grupe 
    ON oglasi.id=grupe.id_oglas
    WHERE oglasi.sub_category = ? AND oglasi.is_approved=1
    GROUP BY oglasi.id
    ORDER BY oglasi.id ASC
    /* LIMIT 24 */';

    private $SELECT_HOME =
    'SELECT  oglasi.id, oglasi.grad, oglasi.naziv, oglasi.naziv_url, oglasi.category, oglasi.sub_category, oglasi.adresa, oglasi.mala_slika, oglasi.treneri, GROUP_CONCAT(DISTINCT grupe.uzrast_od, "-", grupe.uzrast_do SEPARATOR ", ") AS uzrast
    FROM oglasi
    INNER JOIN grupe 
    ON oglasi.id=grupe.id_oglas
    WHERE oglasi.is_approved=1
    GROUP BY oglasi.id
    ORDER BY oglasi.id DESC
    /* LIMIT 24 */';
    public function getHomeData($sub_category = NULL){
        try {
            if ($sub_category) {
                $statement = $this->db->prepare($this->SELECT_HOME_subCat);
                $statement->bindValue(1,$sub_category);
            }
            else{
                $statement = $this->db->prepare($this->SELECT_HOME);                
            }
            $statement->execute();
            return $statement->fetchAll();
        } catch (PDOException $e) {
            f_ajaxReturn(8001,"query Exception ", $e->getCode()." ".$e->getMessage());
        }
    }

    private $SELECT_HOME_unVer = 
    'SELECT  oglasi.id, oglasi.grad, oglasi.naziv, oglasi.naziv_url, oglasi.category, oglasi.sub_category, oglasi.adresa, oglasi.mala_slika, oglasi.treneri, GROUP_CONCAT(DISTINCT grupe.uzrast_od, "-", grupe.uzrast_do SEPARATOR ", ") AS uzrast           
    FROM oglasi
    INNER JOIN grupe 
    ON oglasi.id=grupe.id_oglas
    WHERE oglasi.is_approved=0
    GROUP BY oglasi.id
    ORDER BY oglasi.id DESC';
    public function getHomeData_unVer($sub_category = NULL){
        try {
            $statement = $this->db->prepare($this->SELECT_HOME_unVer);
            $statement->execute();
            return $statement->fetchAll();
        } catch (PDOException $e) {
            f_ajaxReturn(8002,"query Exception ", $e->getCode()." ".$e->getMessage());
        }
    }

    private $SET_OGLAS_VALID = 
    'UPDATE oglasi SET is_approved=1 WHERE id=?';
    public function setOglasValid($id){
        try {
            $statement = $this->db->prepare($this->SET_OGLAS_VALID);
            $statement->bindValue(1,$id);
            $statement->execute();
            return $statement->rowCount();
        } catch (PDOException $e) {
            f_ajaxReturn(8003,"query Exception ", $e->getCode()." ".$e->getMessage());
        }
    }
    
    private $SELECT_OGLAS_id = 'SELECT * FROM oglasi WHERE id = ?';
    public function getOglas_id($id){
        try {
            $statement = $this->db->prepare($this->SELECT_OGLAS_id);
            $statement->bindValue(1,$id);
            $statement->execute();
            return $statement->fetch();
        } catch (PDOException $e) {
            f_ajaxReturn(8020,"query Exception",$e->getCode()." ".$e->getMessage());
        }
    }
    
    private $SELECT_OGLAS_name = 'SELECT * FROM oglasi WHERE naziv_url = ? and sub_category=?';
    public function getOglas_name($naziv_url, $sub_category){
        try {
            $statement = $this->db->prepare($this->SELECT_OGLAS_name);
            $statement->bindValue(1,$naziv_url);
            $statement->bindValue(2,$sub_category);
            $statement->execute();
            return $statement->fetch();
        } catch (PDOException $e) {
            f_ajaxReturn(8013,"query Exception",$e->getCode()." ".$e->getMessage());
        }
    }
    
    
    private $SELECT_GRUPA_idOglas = '
SELECT 
    grupe.pol, GROUP_CONCAT(DISTINCT grupe.uzrast_od, ", ", grupe.uzrast_do) AS uzrast, 
    GROUP_CONCAT(termini.day SEPARATOR ", ") AS days, 
    GROUP_CONCAT(termini.start SEPARATOR ", ") AS start, 
    GROUP_CONCAT(termini.end SEPARATOR ", ") AS end 
FROM grupe 
INNER JOIN termini ON grupe.id=termini.id_grupa 
WHERE grupe.id_oglas=? 
GROUP BY grupe.id
';
    public function getGrupe_idOglas($id_oglas){
        try {
            $statement = $this->db->prepare($this->SELECT_GRUPA_idOglas);
            $statement->bindValue(1,$id_oglas);
            $statement->execute();
            return $statement->fetchAll();
        } catch (PDOException $e) {
            f_ajaxReturn(8030,"query Exception",$e->getCode()." ".$e->getMessage());
        }
    }
    
    private $SELECT_TERMIN_idGrupa = 'SELECT day, start, end FROM termini WHERE id_grupa = ?';
    public function getTermini_id_grupa($id_grupa){
        try {
            $statement = $this->db->prepare($this->SELECT_TERMIN_idGrupa);
            $statement->bindValue(1,$id_grupa);
            $statement->execute();
            return $statement->fetchAll(PDO::FETCH_NUM);
        } catch (PDOException $e) {
            f_ajaxReturn(8004,"query Exception",$e->getCode()." ".$e->getMessage());
        }
    }
    
    
    private $INSERT_OGLAS = "INSERT INTO oglasi(naziv, naziv_url, category, sub_category, grad, deo_grada, adresa, koordinate, treneri, cene, mala_slika, slike, video, social) 
                            VALUES (:naziv, :naziv_url, :category, :sub_category, :grad, :deo_grada, :adresa, :koordinate, :treneri, :cene, :mala_slika, :slike, :video, :social);";
    public function insertOglas($naziv, $naziv_url, $category, $sub_category, $grad, $deo_grada, $adresa,
        $koordinate, $cene, $video, $mala_slika,
        $json_treneri, $json_social, $json_slike
        ){
        try {
            $statement = $this->db->prepare($this->INSERT_OGLAS);
            $statement->bindValue(":naziv",$naziv);
            $statement->bindValue(":naziv_url",$naziv_url);
            $statement->bindValue(":category",$category);
            $statement->bindValue(":sub_category",$sub_category);
            $statement->bindValue(":grad",$grad);
            $statement->bindValue(":deo_grada",$deo_grada);
            $statement->bindValue(":adresa",$adresa);
            $statement->bindValue(":koordinate",$koordinate);
            $statement->bindValue(":treneri",$json_treneri);
            $statement->bindValue(":cene",$cene);
            $statement->bindValue(":mala_slika",$mala_slika);
            $statement->bindValue(":slike",$json_slike);
            $statement->bindValue(":video",$video);
            $statement->bindValue(":social",$json_social);
            $statement->execute();
            return $this->db->lastInsertId();
            # return $statement->rowCount(); // ne radi kad ima begin/commit
        } catch (PDOException $e) {
            f_ajaxReturn(8050,"query Exception",$e->getCode()." ".$e->getMessage());
        }
    }
    
    
    private $INSERT_GRUPA = "INSERT INTO grupe(id_oglas, pol, uzrast_od, uzrast_do)
                             VALUES (:id_oglas, :pol, :uzrast_od, :uzrast_do);";
    public function insertGrupa($id_oglas, $pol, $uzrast_od, $uzrast_do){
            try {
                $statement = $this->db->prepare($this->INSERT_GRUPA);
                $statement->bindValue(":id_oglas",$id_oglas);
                $statement->bindValue(":pol",$pol);
                $statement->bindValue(":uzrast_od",$uzrast_od);
                $statement->bindValue(":uzrast_do",$uzrast_do);
                $statement->execute();
                return $this->db->lastInsertId();
                # return $statement->rowCount(); // ne radi kad ima begin/commit
            } catch (PDOException $e) {
                f_ajaxReturn(8060,"query Exception",$e->getCode()." ".$e->getMessage());
            }
    }
    
    
    private $INSERT_TERMIN = "INSERT INTO termini(id_grupa, day, start, end)
                             VALUES (:id_grupa, :day, :start, :end);";
    public function insertTermin($id_grupa, $day, $start, $end){
        try {
            $statement = $this->db->prepare($this->INSERT_TERMIN);
            $statement->bindValue(":id_grupa",$id_grupa);
            $statement->bindValue(":day",$day);
            $statement->bindValue(":start",$start);
            $statement->bindValue(":end",$end);
            $statement->execute();
            //return $this->db->lastInsertId();
            # return $statement->rowCount(); // ne radi kad ima begin/commit
        } catch (PDOException $e) {
            f_ajaxReturn(8070,"query Exception",$e->getCode()." ".$e->getMessage());
        }
    }
    
    
}
?>