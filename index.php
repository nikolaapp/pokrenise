<?php
$url = urldecode($_SERVER['REQUEST_URI']);
$url_path = substr($url, strrpos($url, ".rs/")+1);
$url_path = explode('/',$url_path,3);

$grad = $url_path[0];
$sub_category = $url_path[1];
$name = $url_path[2];

/* 
var_dump($grad);
var_dump($sub_category);
var_dump($name); */


?>
<!DOCTYPE html>
<html lang="sr">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="robots" content="index, follow"> 
    <meta http-equiv="Cache-Control" content="public"/>
    <!-- <link rel="icon" href="<%= BASE_URL %>favicon.ico"> -->
    
    <title>pokrenise.rs - <?=$grad." ".$sub_category." ".$name?></title>
    <meta name="description" content="Sport, rekreacija, učenje jezika, obuke, kursevi...ko, gde, kada - sve informacije na jednom mestu. Fudbal, odbojka, teretana, fitnes, strani jezici, engleski, nemački, obuke i kursevi, programiranje, dodatna nastava.">
    <meta name="keywords" content="pokrenise, treniraj, sport, fudbal, odbojka, teretana, fitnes, jezici, engleski, nemacki, nemački, trener, profesor, kraljevo, beograd, uzrast, cena">
    <meta name="abstract" content="Sport, rekreacija, učenje jezika, obuke, kursevi...ko, gde, kada - sve informacije na jednom mestu">
    
    <meta itemprop="name">
    <meta property="og:title">    
    <meta name="twitter:title">
    <meta itemprop="description">
    <meta property="og:description">
    <meta name="twitter:description">
    
    <meta name="generator" content="Vue.js">
    <meta name="rating" content="General">
    
    <meta itemprop="image" content="/favicon-96x96.png">
    <meta property="og:image" content="/favicon-96x96.png">
    <meta name="twitter:image" content="/favicon-96x96.png">
    
    <meta name="ICBM" content="20.687254, 43.723848">
    <meta name="geo.position" content="20.687254;43.723848">
    <meta name="geo.region" content="RS">
    <meta name="geo.placename" content="Kraljevo">

    <meta name="author" content="WebCodeAndHost">
    <meta name="web_author" content="WebCodeAndHost">
    <meta name="copyright" content="WebCodeAndHost"> 
    <link rel="me" href="https://WebCodeAndHost.com" type="text/html">
    <link rel="me" href="mailto:nikola.webcodeandhost@gmail.com">
<!-- ---------------------- -->
<meta property="og:type" content="website">
<meta property="og:site_name" content="pokrenise.rs">
<meta property="og:title" content="Saznaj ko su treneri/predavači, koji su termini, lokacije, cene, pogledaj slike i video klipove... pokrenise.rs">
<meta property="og:title" content="treneri, termini, lokacije, cene...">
<meta property="og:description" content="Sport, rekreacija, učenje jezika, obuke, kursevi...ko, gde, kada - sve informacije na jednom mestu">
<meta property="og:url" content="https://pokrenise.rs">
<meta property="og:locale" content="RS">
<meta property="og:image" content="https://pokrenise.rs/og_icon.png">
<meta property="og:image:width" content="1537">
<meta property="og:image:height" content="902">
<!-- ---------------------- -->
<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">

<meta name="msapplication-TileColor" content="#846340">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#846340">
<!-- ---------------------- -->
</head>
<?php

include "index.html";
?>