<?php

/**
 * PROVERA PRISTUPA FAJLOVIMA
 */

/**
 * Provera da li je dozvoljeno da se direktno pristupi fajlu
 * - poredjenje adrese fajla i url adrese
 */
function f_check_direct_file_access(){
    if ( $_SERVER['REQUEST_METHOD']=='GET' && realpath(__FILE__) == realpath( $_SERVER['SCRIPT_FILENAME'] ) ) {
        // url adresa se poklata sa adresom fajla
        // ne radi kada se includuje u drugi fajl - pa mora da se stavlja u svaki fajl posebno
        f_neovlascen_pristup_fajlu("__FILE__ == SCRIPT_FILENAME");
    }
}

/**
 * Provera da li je dozvoljeno da se direktno pristupi fajlu
 * - provera @IS_FILE_INCLUDED konstante
 */
function f_check_top_file(){
    if (! defined('IS_FILE_INCLUDED' /*ovde mora pod navodnicima*/) ){
        // nije definisana konstanta IS_FILE_INCLUDED - definise se preko kontrolera
        f_neovlascen_pristup_fajlu("IS_FILE_INCLUDED");
    }
}

/**
 * Provera da li je dozvoljeno da se direktno pristupi fajlu
 * - provera $_SESSION['user']
 */
function f_check_user(){
    if( ! isset($_SESSION['user']) )  {
        // korisnik nije logovan 
        $_SESSION['msg'] = "007 - niste prijavljeni";
        exit(header("location: ../view/login.php"));
    }
}

/**
 * Akcija pri neovljascenom pristupu fajlu
 */
function f_neovlascen_pristup_fajlu($msg){
    //echo "<br><br>$msg<br><br>";
    
    //Up to you which header to send, some prefer 404 even if
    //the files does exist for security
    exit(header( 'HTTP/1.0 404 Not Found', TRUE, 404 ));
    
    //OR choose the appropriate page to redirect users
    //exit( header( 'location: ../error.html' ) );
}

?>