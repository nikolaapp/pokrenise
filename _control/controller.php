<?php

session_start();

### ucitavanje f_requireOnce_handler()
require_once '../_control/requireOnce_handler.php';

### ucitavanje root putanje
f_requireOnce_handler( '../_control/root_config.php');
### ucitavanje defined_vars
f_requireOnce_handler( '../_control/defined_vars.php');


### ucitavanje funkcija
f_requireOnce_handler( '../_control/shared_func.php');

$action = f_readPhpInput('action') ? f_readPhpInput('action') : "";
if(empty($action)) $action = !empty(f_readPost('action'))    ?   f_readPost('action') : "";
if(empty($action)) $action = !empty(f_readGet('action'))    ?   f_readGet('action')   : "";

if (! empty($action)) {
    f_requireOnce_handler( ROOT . '_model/DAO.php');
    $dao = new DAO();
    
    switch ($action) {
        case "getHomeData":
            $sub_category = f_readPhpInput('sub_category');
            
            if (empty($sub_category)){
                //-----ok, uzimam rand listu za home stranicu
                //f_ajaxReturn(9012,"sub_category not set");
            }
            
            $out_data = $dao->getHomeData($sub_category);
            if (sizeof($out_data) ==0 ){
                f_ajaxReturn(-101,"još nema oglasa u kategoriji <strong>$sub_category</strong>");
            }
            
            f_ajaxReturn(0, "podaci su spremni", $out_data);
            break;
////////////////////////////////////////////////////////////////////////
        case "getOglasById":
            $id = (int) f_readPhpInput('id');
            
            if (empty($id) || !is_int($id) || $id<1){
                f_ajaxReturn(9011,"invalid id",$id);
            }            
            
            $out_data = array();
            $out_data['oglas'] = $dao->getOglas_id($id);
            $out_data['grupe'] = $dao->getGrupe_idOglas($id);
            
            f_ajaxReturn(0, "podaci su spremni", $out_data);
            break;
////////////////////////////////////////////////////////////////////////
        case "getOglasByName":
            $naziv_url = f_readPhpInput('naziv_url');
            $sub_category = f_readPhpInput('sub_category');
            
            if (empty($naziv_url) || empty($sub_category)){
                f_ajaxReturn(9012,"missing name/sub_category selector", "$naziv_url $sub_category");
            }        
            
            $out_data = array();
            $out_data['oglas'] = $dao->getOglas_name($naziv_url, $sub_category);
            
            if ( !empty($out_data['oglas'])) {
                $out_data['grupe'] = $dao->getGrupe_idOglas($out_data['oglas']['id']);
                f_ajaxReturn(0, "podaci su spremni", $out_data);
            }
            else{
                f_ajaxReturn(-1, "Ne postoji oglas na zadatom linku!");
            }
            
            break;        
////////////////////////////////////////////////////////////////////////        
        case 'uploadOglas':
            $grad = readFormData("grad");
            $deo_grada = readFormData("deo_grada");
            $adresa = readFormData("adresa");
            $category = readFormData("category");
            $sub_category = readFormData("sub_category");
            $naziv = readFormData("naziv");
            $cene = readFormData("cene");
            $video = readFormData("video");
            $koordinate = readFormData("koordinate"); 
            # ne treba --- $mala_slika = readFormData("mala_slika");           
            
            $test = validateStringLength($grad, $deo_grada, $adresa, $category,
                $sub_category, $naziv, $cene, $video, $koordinate);
            if (sizeof($test) > 0)  {                
                f_ajaxReturn(9901,"Dužine unetih stringova nisu odgovarajuće.", $test);
            }

            $naziv = str_replace('&quot;', '', $naziv);
            $naziv = str_replace("&apos;", '', $naziv);
            $adresa = ucwords(strtolower($adresa));

            $naziv_url = createNazivUrl($naziv);
            $namecheck = $dao->getOglas_name($naziv_url, $sub_category);
            if (empty($namecheck)) {
                //"naziv je validan"
            }
            else{
                f_ajaxReturn(9967, "Naziv <strong>$naziv</strong> već postoji u podkategoriji <strong>$sub_category</strong> - promenite naziv!");
            }
            
            
            $treneri = readFormData("treneri");
            
            if ( ! is_array($treneri) || sizeof($treneri)==0) {
                f_ajaxReturn(9902,"Podaci o trenerima nisu lepo uneti.", $treneri);
            }
            
            foreach ($treneri as $element) {
                $test = validateLength($element['ime'], 4, 32);
                if ($test['code'] > 0) 
                    f_ajaxReturn(9903,"Nedozvoljena dužina imena ".$element['ime'], $element['ime']);
                
                $test = validateLength($element['telefon'], 8, 10);
                if ($test['code'] > 0) 
                    f_ajaxReturn(9903,"Nedozvoljena dužina broja telefona ".$element['telefon'], $element['telefon']);
                    
            }
            
            $json_treneri = json_encode($treneri);
            # $test = validateLength($json_treneri, 10, 255); --- nepotrebno
            # if ($test['code'] > 0) $out_msg[] = $test;
                        
            
            $social = readFormData("social");
            $json_social = json_encode($social);
            $test = validateLength($json_social, 0, 1024);
            if ($test['code'] > 0) f_ajaxReturn(9904,"Nedozvoljena dužina adresa društvenih mreža.", $social);
                
                        
            $grupe = readFormData("grupe");
            
            foreach ($grupe as $grup) {
                if ( ! (sizeof($grup['days'])==sizeof($grup['start']) &&
                    sizeof($grup['start'])==sizeof($grup['end']) )) {
                        f_ajaxReturn(9905,"Podaci o grupama nisu kompletni.", $grup);
                }
                
                if (sizeof($grup['uzrast']) != 2) {
                    f_ajaxReturn(9905,"Podaci o uzrastu nisu kompletni.", $grup);
                }
                
                $min = (int) $grup['uzrast'][0];
                $max = (int) $grup['uzrast'][1];
                if ( ! is_int($min) ||  $min<1 || $min>$max) {
                    f_ajaxReturn(9906,"Podaci o uzrastu nisu validni.", $grup);
                }
                if ( ! is_int($max) ||  $max>21) {
                    f_ajaxReturn(9907,"Podaci o uzrastu nisu validni.", $grup);
                }
                
                $pol = (int)$grup['pol'];
                if ( ! is_int($pol) || $pol<0 || $pol>2) {
                    f_ajaxReturn(9908,"Podaci o polu nisu validni.", $grup);
                }
                
            }
            //print_r($grupe);
            
            
            $slike = isset($_FILES['slike'])?$_FILES['slike']:"";
            $niz_path_slike = array();
            $mala_slika ="";
            //print_r($slike);
            
            $folder_name = getFolderName($naziv, $sub_category);            
            $name_separator = " "; 
            
            for ($i = 0; $i < sizeof($slike['name']); $i++) {                
                $name = $slike['name'][$i];                              
                if($slike['size'][$i] > 8388608){
                    f_ajaxReturn(9909,"Veličina fajla $name je veća od dozvoljenih 8MB.");
                }
                               
                $file_type= getFileType($name); 
                $file_name = 
                    "pokrenise".$name_separator
                    .$sub_category.$name_separator
                    .removeSpecChars($naziv).$name_separator
                    ."$i.$file_type";
                
                $filePathName = createFilePathAndName($file_name, $folder_name);
                
                if (move_uploaded_file($slike['tmp_name'][$i], $filePathName) == false) {
                    f_ajaxReturn(9910, "Neuspešan upload fajla $name");
                } 

                if (! $mala_slika) {
                    $mala_slika = make_thumb($filePathName);
                }
                ### vraca se jos jedan korak unazad, jer imam root/KRALJEVO/oglas
                $niz_path_slike[]= "../".$filePathName;
            }
            $json_slike = json_encode($niz_path_slike);

            if (! $mala_slika) {
                # nije kreirana niti jedna mala slika - uzimamo original koji je najmanje velicine
                $mala_slika = $niz_path_slike[findIndexOfSmallest($slike)];
            }

            try {
                $id_oglas = $dao->insertOglas($naziv, $naziv_url, $category, $sub_category, $grad, $deo_grada, $adresa, 
                                                $koordinate, $cene, $video, $mala_slika, 
                                                $json_treneri, $json_social, $json_slike);
                
                if ($id_oglas > 0) {                    
                    foreach ($grupe as $single_grupa) {
                        $id_grupa = $dao->insertGrupa($id_oglas, $single_grupa['pol'], $single_grupa['uzrast'][0], $single_grupa['uzrast'][1]);
                        
                        if ($id_grupa > 0) {                        
                            for ($i = 0; $i < sizeof($single_grupa['days']); $i++) {
                                $dao->insertTermin($id_grupa, $single_grupa['days'][$i], $single_grupa['start'][$i], $single_grupa['end'][$i]);
                            }
                        }
                        else{
                            f_ajaxReturn(9960,"insert fault, nije uneta grupa za: $naziv");
                        }
                    
                    }

                    $upload_contact = readFormData("upload_contact");
                    f_requireOnce_handler( ROOT . '_model/CreateMail.php');
                    $mailer = f_createMail($naziv, "https://pokrenise.rs/kraljevo?admin=nikola.ufW45TuF23s4", "https://pokrenise.rs/_control/controller.php?action=SetOglasValid&id=$id_oglas",  $upload_contact);
                    if ($mailer !== 0) {
                        //f_ajaxReturn(5555, "greska sa mejlom", $mailer);                    
                    }
                    
                    f_ajaxReturn(0,"unet je oglas za: $naziv");
                }
                else{
                    f_ajaxReturn(9950,"insert fault, nije unet oglas: $naziv");
                }
                
            } catch (Exception $e) {
                f_ajaxReturn(9952, "insert exception, nije unet oglas: $naziv");
            } 

            break;    
////////////////////////////////////////////////////////////////////////
        case 'ValidateNaziv':
            $sub_category = f_readPhpInput("sub_category");
            $naziv = f_readPhpInput("naziv");

            if (empty($naziv) || empty($sub_category)){
                f_ajaxReturn(9064,"missing name/sub_category selector", "$naziv_url $sub_category");
            }  

            $naziv_url = createNazivUrl($naziv);

            $out_data = $dao->getOglas_name($naziv_url, $sub_category);

            if (empty($out_data)) {
                f_ajaxReturn(0, "naziv je validan", true);
            }
            else{
                f_ajaxReturn(9966, "Naziv <strong>$naziv</strong> već postoji u podkategoriji <strong>$sub_category</strong> - promenite naziv!");
            }

            break;
////////////////////////////////////////////////////////////////////////
        case 'getHomeData_unVer':  
            $user = f_readPhpInput("user");
            switch ($user) {
                case 'nikola.ufW45TuF23s4':
                    $out_data = $dao->getHomeData_unVer();
                    if (sizeof($out_data) ==0 ){
                        f_ajaxReturn(4444,"nema neverifikovanih oglasa");
                    }
                    
                    f_ajaxReturn(0, "podaci su spremni", $out_data);
                    break;
                    
                default:            
                    f_ajaxReturn(1111, "fuckoff");
                    break;
            }

            break;
////////////////////////////////////////////////////////////////////////
        case 'SetOglasValid':
            echo "<h1>ADMIN PORUKA</h1>";
            $home = "<br><h2><a href='//pokrenise.rs'>idi na HOME page</a></h2>";
            
            $id = !empty($_GET['id']) ? (int)$_GET['id'] : "";

            if (!is_int($id)) {
                echo "<h2 style='color:red;'>Oglas NIJE odobren, jer je prosledjen nepravilan id: -$id-</h2>";
                echo $home;
                return;
            }

            $oglas = $dao->getOglas_id($id);
            if (empty($oglas)) {
                echo "<h2 style='color:red;'>Oglas sa id: -$id- NEPOSTOJI!</h2>";
            } 
            else if(!empty($oglas['is_approved']) && $oglas['is_approved']==1){
                $naziv = $oglas['naziv'];
                echo "<h2 style='color:blue;'>Oglas sa id: -$id- je već odobren -> $naziv</h2>";
            }
            else {
                $naziv = $oglas['naziv'];
                $rowcount = $dao->setOglasValid($id);

                if ($rowcount == 1) {
                    echo "<h2 style='color:green;'>Uspešno odobren oglas sa id: -$id- -> $naziv</h2>";
                } else {
                    echo "<h2 style='color:red;'>GREŠKA, ukupno promenjeno: -$rowcount- ,id: -$id- -> $naziv </h2>";
                }
            }              

            echo $home;

            break;
////////////////////////////////////////////////////////////////////////
        default:
            f_ajaxReturn(9002,"invalid action", $action);
            break;
    }
}
else {
    f_ajaxReturn(9001,"no action");
}

function getFolderName($file, $sub_category){

    $file = removeSpecChars($file);
    $sub_category = removeSpecChars($sub_category);  
    
    $folderpath = $sub_category.'/'.$file;
    
    /* if (mb_strlen($folderpath, 'UTF-8')>127){
        $file_type= getFileType($file_name); 
        $file_name = substr($file_name,0,120).'.'.$file_type;
    } */
    
    return $folderpath;
}

function getFileType($file_name){
    return strtolower(end(explode('.',$file_name))); 
}

function findIndexOfSmallest($slike){
    $index_of_min = 0;
    for ($i=0; $i < sizeof($slike['size']); $i++) { 
        if($slike['size'][$i] < $slike['size'][$index_of_min]) {
            $index_of_min = $i;
        }
    }    
    return $index_of_min;    
}

function validateStringLength($grad, $deo_grada, $adresa, $category, 
    $sub_category, $naziv, $cene, $video, $koordinate){
    
    $out_msg = array();
    
    $test = validateLength($grad, 2, 32);
    if ($test['code'] > 0) $out_msg[] = $test;
    
    $test = validateLength($deo_grada, 2, 32);
    if ($test['code'] > 0) $out_msg[] = $test;    
    
    $test = validateLength($adresa, 4, 64);
    if ($test['code'] > 0) $out_msg[] = $test;
    
    $test = validateLength($category, 3, 32);
    if ($test['code'] > 0) $out_msg[] = $test;
    
    $test = validateLength($sub_category, 3, 64);
    if ($test['code'] > 0) $out_msg[] = $test;
    
    $test = validateLength($naziv, 3, 64);
    if ($test['code'] > 0) $out_msg[] = $test;
    
    $test = validateLength($cene, 3, 256);
    if ($test['code'] > 0) $out_msg[] = $test;
    
    $test = validateLength($video, 0, 128);
    if ($test['code'] > 0) $out_msg[] = $test;
    
    $test = validateLength($koordinate, 0, 32);
    if ($test['code'] > 0) $out_msg[] = $test;
    
    return $out_msg;    
    
}

function validateLength($string, $min_length, $max_length){
    if (! is_string($string)) {
        return array("code"=>9991, "msg"=>"nepravilan format podatka");
    }
    elseif (mb_strlen($string, 'utf8') < $min_length  ||  mb_strlen($string, 'utf8') > $max_length ) {
        return array("code"=>9992, "msg"=>"broj unetih karaktera mora biti izmedju $min_length i $max_length");
    }
    else{
        return array("code"=>0);
    }
}

function readFormData($selector){
    return !empty($_POST[$selector]) ? f_check_input($_POST[$selector]) : '';
}

function make_thumb($src) {
    //todo fixme ne radi lepo
    $imageFileType = strtolower(pathinfo($src,PATHINFO_EXTENSION));
    //echo "imageFileType $imageFileType";
    switch($imageFileType) {
        case 'jpg':
        case 'jpeg':
            $source_image = imagecreatefromjpeg($src);
            break;
        case 'gif':
            $source_image = imagecreatefromgif($src);
            break;
        case 'png':
            $source_image = imagecreatefrompng($src);
            break;
        case 'bmp':
        //da ne da radi
            //$source_image = imagecreatefrombmp_moje($src);
            //break;
        default:
            //echo "imageFileType $imageFileType";
            return false;
    }
    $dest= substr($src, 0, strrpos( $src, '.'))."_thumb.jpg";
    $desired_width = 300;

    try {
        $width = imagesx($source_image);
        $height = imagesy($source_image);
        //echo "width $width - height$height";

        /* find the "desired height" of this thumbnail, relative to the desired width  */
        $desired_height = floor($height * ($desired_width / $width));
        //echo "desired_height $desired_height";
        
        
        /* create a new, "virtual" image */
        $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
        
        /* copy source image at a resized size */
        imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
        
        /* create the physical thumbnail image to its destination */
        imagejpeg($virtual_image, $dest);

        if(is_readable ( $dest )) {
            return $dest;
        }
        else {
            return false;
        }
    } catch (Exception $e) {
        return false;
    }
    
}
function imagecreatefrombmp_moje( $fileName ) {
    // ne radi lepo - kreira bezveze sliku
    $file = fopen($fileName, "rb");
    $read = fread($file, 10);
    while (!feof($file) && ($read <> ""))
        $read .= fread($file, 1024);
    $temp = unpack("H*", $read);
    $hex = $temp[1];
    $header = substr($hex, 0, 108);
    if (substr($header, 0, 4) == "424d") {
        $parts = str_split($header, 2);
        $width = hexdec($parts[19] . $parts[18]);
        $height = hexdec($parts[23] . $parts[22]);
        unset($parts);
    }
    $x = 0;
    $y = 1;
    $image = imagecreatetruecolor($width, $height);
    $body = substr($hex, 108);
    $body_size = (strlen($body) / 2);
    $header_size = ($width * $height);
    $usePadding = ($body_size > ($header_size * 3) + 4);
    for ($i = 0; $i < $body_size; $i+=3) {
        if ($x >= $width) {
            if ($usePadding)
                $i += $width % 4;
            $x = 0;
            $y++;
            if ($y > $height)
                break;
        }
        $i_pos = $i * 2;
        $r = hexdec($body[$i_pos + 4] . $body[$i_pos + 5]);
        $g = hexdec($body[$i_pos + 2] . $body[$i_pos + 3]);
        $b = hexdec($body[$i_pos] . $body[$i_pos + 1]);
        $color = imagecolorallocate($image, $r, $g, $b);
        imagesetpixel($image, $x, $height - $y, $color);
        $x++;
    }
    fclose($file);
    unset($body);
    return $image;
}


/**
 * funkcija koja kreira folder u kojem ce se cuvati slike iz upload-a
 * @return string
 */
function createFilePathAndName($file_name, $folder_name){
    //$separator = DIRECTORY_SEPARATOR;
    $separator = '/';
    $filepath = '../_slike'.$separator.$folder_name;
    //echo $filepath;
    createFolder($filepath);
    if (is_dir($filepath)){   
       return $filepath.$separator.$file_name; 
    }
    else return '';
}


/**
 * funkcija za kreiranje foldera
 */
function createFolder($path) {
    if (!is_dir($path)) {
        mkdir($path, 0777, true);
    }
}

function createNazivUrl($naziv){
    return  str_replace(" ", "-", $naziv);
}

function removeSpecChars($inputString){
    $inputString = str_replace(' ', '_', $inputString);

    // [^ Match any character that is not in the set.
    $inputString = preg_replace('/[^-_a-zA-Z0-9ČčĆćŽžŠšĐđ]/', '', $inputString);
        
    $inputString = str_replace('nbsp', '_', $inputString);
    $inputString = str_replace('quot', '', $inputString);
    $inputString = str_replace("apos", '', $inputString);

    if (empty($inputString)) {
        $inputString = rand(10000,99999);
    }
    //echo "<br>".$inputString;
    return $inputString; 
}


?>