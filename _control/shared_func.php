<?php

const NOV_RED = "<br><br>";

/**
 * Provera poslate parametre - zastita W3S
 * @todo omoguci provery za nizove
 * @param variant $data
 * @return variant checked data
 */
function f_check_input($data) {
    if (! is_array($data) ){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        // nece na azure da radi a i ne treba mi jer koristip PDO prepared statments
        //$data = mysql_real_escape_string($data);
    }
    else {
        ### ??????
    }
    return $data;
}

/**
 * Ocitavanje parametra poslatog POST
 * @param string $name
 * @param bool $isJson
 * @return string
 */
function f_readPost($name, $isJson=false) {
    if (is_string($name)) 
        if ($isJson) 
            //ako je u pitanju JSON string
            return isset($_POST[$name]) ? f_check_input(json_decode($_POST[$name],true)) : "";
        else 
            return isset($_POST[$name]) ? f_check_input($_POST[$name]) : "";
    else
        return "";    
}


/**
 * Ocitavanje (file_get_contents('php://input')) parametra poslatog kao JSON
 * @param string $name
 * @return string
 */
function f_readPhpInput($name) {
    if (is_string($name)){
        $input = file_get_contents('php://input');
        if (f_isJson($input)){
            //ako je u pitanju JSON string            
            $input_data = json_decode($input, TRUE);
            return isset($input_data[$name]) ? f_check_input($input_data[$name]) : "";
        }else{
            return isset($input[$name]) ? f_check_input($input[$name]) : "";
        }
    }else return "";
}

/**
 * Provera true/false da li je obj JSON ili ne
 * @param $string 
 * @return boolean
 */
function f_isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}

/**
 * Ocitavanje parametra poslatog GET
 * @param string $name
 * @return string
 */
function f_readGet($name) {
    if (is_string($name))
        return isset($_GET[$name]) ? f_check_input($_GET[$name]) : "";
    else
        return "";
}

/**
 * AJAX - Forma povratnih rezultata od servera ka useru 
 * @param int $status (0 - sve ok)
 * @param string $msg
 * @param variant $data
 */
function f_ajaxReturn($status, $msg=NULL, $data=NULL){ 
    $jsonObj = array();
    $jsonObj["status"] = $status;
    
    if (defined('DEBUG_LEVEL') && DEBUG_LEVEL>0){ 
        // dev nivo - vracamo i msg        
        $jsonObj["msg"] = $msg;
    }
    else {
        //nivo produkcije
        $jsonObj["msg"] = "podaci nisu trenutno dostupni";
    }
    
    $jsonObj["data"] = $data;
    
    echo json_encode($jsonObj); 
    exit;
    //errLog($status, $msg);
    
}

/**
 * Proverava da li nazivi pripadaju dozvoljeniNazivi
 * @param String $nazivi
 * @param String[] $dozvoljeniNazivi
 * @return number -1 znaci da je sve ok
 */
function f_proveraNaziva($nazivi, $dozvoljeniNazivi){
    foreach ($dozvoljeniNazivi as &$value) {
        $value = mb_strtolower($value, 'UTF-8');
    }
    if (is_array($nazivi)) {
        $index = 0;
        foreach ($nazivi as $naziv) {
            if (! in_array(mb_strtolower($naziv, 'UTF-8'), $dozvoljeniNazivi)) {
                return $index; // greska
            }
            $index++;
        }
    }
    else{ //string
        if (! in_array(mb_strtolower($nazivi, 'UTF-8'), $dozvoljeniNazivi)) {
            return 10101; // greska
        }
    }
    
    return -1; //sve ok
}

function getCurentAzurePort(){
    $portPath = "../../../data/mysql/";
    $portFileName = "MYSQLCONNSTR_localdb.txt";
    $file = $portPath.$portFileName;
    if (is_file($file)){
        $handle = fopen($file, "r");
        if(!$handle){
            return "ERROR:cant open file";
        }else{
            $fileContent = fread ($handle,filesize($file));
            fclose($handle);
            preg_match('~:(.*?);~', $fileContent, $output);
            if (is_array($output) && isset($output[1])) {
                $port = (int) $output[1];
                if (is_int($port) && $port>50000 && $port <60000) {
                    return $port;
                }
            }
            return "ERROR: can not read port value: $fileContent";
        }
    }
    return "ERROR: file don't exists";
}


function errLog($code, $msg){
    if (isset($_SERVER['REMOTE_ADDR'])){
        $remote_addr = $_SERVER['REMOTE_ADDR'];
    }else{
        $remote_addr = 'UNKNOWN';
    }
    if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
        $http_x_forw = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $http_x_forw = 'UNKNOWN';
    }
    if (isset($_SERVER['REQUEST_URI'])){
        $url = $_SERVER['REQUEST_URI'];
    }else{
        $url = 'UNKNOWN';
    }
    if (isset($_SESSION['user']['ime'])){
        $user = $_SESSION['user']['ime'];
    }else{
        $user = 'UNKNOWN';
    }
    $file = basename(__FILE__);
    
    if ($code === 0) $code = "success";
    if (empty($msg))  $msg  = "UNKNOWN";
    
#     f_requireOnce_handler(ROOT.'model/DAOerrLog.php');
#     $dao = new DAOerrLog();
#     $dao->insertErrLog($code, $msg, $user, $remote_addr, $http_x_forw, $url, $file);
                        
}


?>