<?php
/**
 * test i handle require_once naredbe
 * --- prvo sto ucitavamo u svakom fajlu da bi u tom fajlu nadalje 
 * mogao da koristim ovu f-ju
 * --- stavio sam ga u control folder da bi mogao da ga koristim 
 * i za ROOT konstantu
 * @param string $path do fajla koji ucitavamo
 */
function f_requireOnce_handler($path){
    if (is_readable($path) ){
        require_once $path;
    }
    else {
        //f_posaljiRezAjax("101", "nedostupan fajl $path");
        
        echo "Nije moguce ucitati fajl: <h3>$path</h3>";
        
        exit();
    }
}

?>