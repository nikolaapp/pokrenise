<?php

/**
 * PROVERA PRISTUPA FAJLOVIMA
 * definicija TOP_FILE varijable za potrebe svih kontrolera
 * a ostali fajlovi ce imati definisan IS_FILE_INCLUDED samo ako su includovani 
 * preko kontolera
 * ako nisu onda IS_FILE_INCLUDED nije definisan i pokrece se zastita iz access_file
 */
define('IS_FILE_INCLUDED',TRUE);

/**
 * 0-production level
 * 1-localhost level 
 * @var int 
 */
define('DEBUG_LEVEL',1);


?>