-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 22, 2019 at 12:35 AM
-- Server version: 5.6.43-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webcodea_pokrenise`
--

-- --------------------------------------------------------

--
-- Table structure for table `grupe`
--

CREATE TABLE `grupe` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `id_oglas` smallint(5) UNSIGNED NOT NULL,
  `pol` tinyint(1) NOT NULL,
  `uzrast_od` tinyint(1) UNSIGNED NOT NULL,
  `uzrast_do` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `grupe`
--

INSERT INTO `grupe` (`id`, `id_oglas`, `pol`, `uzrast_od`, `uzrast_do`) VALUES
(1, 1, 1, 16, 18),
(2, 1, 0, 16, 21),
(3, 2, 1, 7, 13),
(4, 2, 2, 7, 13),
(5, 2, 2, 14, 17),
(6, 3, 0, 6, 15),
(28, 27, 0, 6, 21);

-- --------------------------------------------------------

--
-- Table structure for table `oglasi`
--

CREATE TABLE `oglasi` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `is_approved` tinyint(1) NOT NULL DEFAULT '0',
  `naziv` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `naziv_url` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `category` enum('sport','umetnost','jezici','nastava','kursevi') COLLATE utf8_unicode_ci NOT NULL,
  `sub_category` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `grad` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `deo_grada` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `adresa` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `koordinate` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `treneri` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `cene` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `mala_slika` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `slike` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `video` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `social` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oglasi`
--

INSERT INTO `oglasi` (`id`, `is_approved`, `naziv`, `naziv_url`, `category`, `sub_category`, `grad`, `deo_grada`, `adresa`, `koordinate`, `treneri`, `cene`, `mala_slika`, `slike`, `video`, `social`, `timestamp`) VALUES
(1, 0, 'FK Karadjordje', 'FK-Karadjordje', 'sport', 'fudbal', 'Kraljevo', 'Ribnica', 'Фудбалски клуб Карађорђе Рибница', '', '[{\"ime\":\"Nikola Nikoli\\u0107\",\"telefon\":\"0641234567\"}]', '20e/mesečno', '../_slike/FK_Karadjordje/pokrenise fudbal FK Karadjordje 0_thumb.jpg', '[\"..\\/..\\/_slike\\/FK_Karadjordje\\/pokrenise fudbal FK Karadjordje 0.jpg\",\"..\\/..\\/_slike\\/FK_Karadjordje\\/pokrenise fudbal FK Karadjordje 1.jpg\"]', 'https://www.youtube.com/watch?v=N79SChjpQEk', '{\"f\":\"https:\\/\\/facebook.com\\/pages\\/category\\/Sports-Team\\/Fudbalski-Klub-Karadjordje-Ribnica-1817057691853656\\/\"}', '2019-04-06 17:44:00'),
(2, 1, 'OK Tehničar', 'OK-Tehničar', 'sport', 'odbojka', 'Kraljevo', 'gradska zona', 'Solunskih ratnika 8/13; 36000 Kraljevo, Srbija', '', '[{\"ime\":\"Dragan \\u0110or\\u0111evi\\u0107\",\"telefon\":\"0638298424\"},{\"ime\":\"Aleksandar Dai\\u0161evi\\u0107\",\"telefon\":\"0641699362\"}]', 'pozovite telefonom za sva pitanja u vezi cena...', '../_slike/OK_Tehničar/pokrenise odbojka OK Tehničar 0_thumb.jpg', '[\"..\\/..\\/_slike\\/OK_Tehni\\u010dar\\/pokrenise odbojka OK Tehni\\u010dar 0.jpg\"]', 'https://www.youtube.com/embed/videoseries?list=PL3E2106627210DE24&amp;hl=en_US', '{\"f\":\"https:\\/\\/sr-rs.facebook.com\\/pages\\/OK-Tehnicar-Kraljevo\\/155197687859423\",\"y\":\"https:\\/\\/www.youtube.com\\/embed\\/videoseries?list=PL3E2106627210DE24&hl=en_US\",\"w\":\"http:\\/\\/oktehnicar.org\\/\",\"e\":\"oktehnicarkv@gmail.com\"}', '2019-04-03 20:09:27'),
(3, 1, 'OK 4.Kraljevački Bataljon', 'OK-4.Kraljevački-Bataljon', 'sport', 'odbojka', 'Kraljevo', 'gradska zona', 'OŠ IV kraljevački bataljon, Olge Jovičić Rite 1, Kraljevo', '', '[{\"ime\":\"Branko Stojanovi\\u0107\",\"telefon\":\"063611544\"},{\"ime\":\"Andrej Stojanovi\\u0107\",\"telefon\":\"063544117\"}]', 'pozovite telefonom za sva pitanja u vezi cena...', '../_slike/OK_4.Kraljevački_Bataljon/pokrenise odbojka OK 4.Kraljevački Bataljon 0_thumb.jpg', '[\"..\\/..\\/_slike\\/OK_4.Kraljeva\\u010dki_Bataljon\\/pokrenise odbojka OK 4.Kraljeva\\u010dki Bataljon 0.jpg\",\"..\\/..\\/_slike\\/OK_4.Kraljeva\\u010dki_Bataljon\\/pokrenise odbojka OK 4.Kraljeva\\u010dki Bataljon 1.jpg\"]', '', '{\"e\":\"andryyy2602@gmail.com\"}', '2019-04-06 17:43:57'),
(27, 1, 'KK ANIMA SAN 2010', 'KK-ANIMA-SAN-2010', 'sport', 'karate', 'Kraljevo', 'gradska zona', 'Olge Jovicic Rite Kraljevo', '', '[{\"ime\":\"Ili\\u0107 Marko\",\"telefon\":\"0642342795\"},{\"ime\":\"Aleksandar Vrani\\u0107\",\"telefon\":\"062847658\"},{\"ime\":\"Nikola Petrovi\\u0107\",\"telefon\":\"0613112203\"}]', '1500 dinara mesečno, za dvoje dece iz porodice 2400, za troje 3000 dinara. Prvi mesec treniranja besplatno.', '../_slike/karate/Kk_quotANIMA_SAN_2010quot/pokrenise karate Kk_quotANIMA_SAN_2010quot 0_thumb.jpg', '[\"..\\/..\\/_slike\\/karate\\/Kk_quotANIMA_SAN_2010quot\\/pokrenise karate Kk_quotANIMA_SAN_2010quot 0.jpg\"]', '', '{\"e\":\"Anima_san@yahoo.com\"}', '2019-04-21 16:26:13');

-- --------------------------------------------------------

--
-- Table structure for table `termini`
--

CREATE TABLE `termini` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `id_grupa` smallint(5) UNSIGNED NOT NULL,
  `day` tinyint(1) NOT NULL,
  `start` time NOT NULL,
  `end` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `termini`
--

INSERT INTO `termini` (`id`, `id_grupa`, `day`, `start`, `end`) VALUES
(1, 1, 1, '12:30:00', '13:30:00'),
(2, 1, 2, '14:00:00', '15:05:00'),
(3, 2, 6, '02:00:00', '03:25:00'),
(4, 3, 2, '19:00:00', '22:00:00'),
(5, 3, 5, '19:00:00', '20:30:00'),
(6, 4, 4, '19:00:00', '22:00:00'),
(7, 4, 6, '10:00:00', '13:00:00'),
(8, 5, 1, '19:00:00', '22:00:00'),
(9, 5, 3, '19:00:00', '22:00:00'),
(10, 5, 5, '19:00:00', '22:00:00'),
(11, 6, 4, '18:00:00', '19:00:00'),
(12, 6, 6, '10:00:00', '12:00:00'),
(13, 6, 7, '19:00:00', '21:00:00'),
(35, 28, 1, '20:00:00', '21:30:00'),
(36, 28, 3, '20:00:00', '21:30:00'),
(37, 28, 5, '20:00:00', '21:30:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `grupe`
--
ALTER TABLE `grupe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_oglas` (`id_oglas`);

--
-- Indexes for table `oglasi`
--
ALTER TABLE `oglasi`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sub_category` (`sub_category`,`naziv`);

--
-- Indexes for table `termini`
--
ALTER TABLE `termini`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_grupa` (`id_grupa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `grupe`
--
ALTER TABLE `grupe`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `oglasi`
--
ALTER TABLE `oglasi`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `termini`
--
ALTER TABLE `termini`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `grupe`
--
ALTER TABLE `grupe`
  ADD CONSTRAINT `grupe_ibfk_1` FOREIGN KEY (`id_oglas`) REFERENCES `oglasi` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `termini`
--
ALTER TABLE `termini`
  ADD CONSTRAINT `termini_ibfk_1` FOREIGN KEY (`id_grupa`) REFERENCES `grupe` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
